var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');


app.listen(port);
console.log("Esperando que funcione la aplicación Polymer desde node " + port);

app.use(express.static(__dirname, + "/build/default"));

app.use(function(req, res, next) {
 res.header("Access-Control-Allow-Origin", "*");
 res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
 next();
});

app.get("/", function(req,res){
	res.sendFile('index.html', {root: '.'});
});